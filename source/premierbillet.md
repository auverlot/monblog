title: Je lance mon blog
time: 2022-01-28T14:22:48+01:00
published_filename: 2022-01-28-je-lance-mon-blog.html
guid: 33b3022d-8fa4-46a8-8057-5f6ae2aac18a
tags: blog,annonce

Bienvenue sur mon blog. Cet article est le premier publié et j’espère que beaucoup d’autres suivront.

![Image d’un journaliste](assets/images/journaliste.png)

N’hésitez pas à visiter régulièrement ce site.

